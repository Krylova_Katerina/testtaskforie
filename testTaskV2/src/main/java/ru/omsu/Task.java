package ru.omsu;

import java.time.LocalTime;
import java.util.Objects;

public class Task {
    private LocalTime startTime;
    private LocalTime endTime;
    private String status;
    private int num;

    public Task(LocalTime startTime, LocalTime endTime, String status, int num) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.status = status;
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Objects.equals(startTime, task.startTime) &&
                Objects.equals(endTime, task.endTime) &&
                Objects.equals(status, task.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(startTime, endTime, status);
    }
}
