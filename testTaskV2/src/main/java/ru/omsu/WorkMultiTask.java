package ru.omsu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class WorkMultiTask extends Work implements Runnable {
    private List<Task> tasks = new ArrayList<>();
    private Thread t;
    private boolean end = true;
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkMultiTask.class);


    public WorkMultiTask(Task task, LocalTime time, int t1, int t2, int t3) {
        super(task, time, t1, t2, t3);
        tasks.add(task);
        t = new Thread(this);
        t.start();
    }

    @Override
    void stopWork() {
        end = false;
    }

    @Override
    public void run() {
        while (end) {
            LocalTime timeNow = null;
            timeNow = timeNow.now();

            if (timeNow.plusMinutes(-T1).getMinute() == time.getMinute()) {
                if (tasks.size() != 0) {
                    task = new Task(timeNow, timeNow.plusMinutes(T2), "not ready", tasks.get(tasks.size()-1).getNum()+1);
                } else {
                    task = new Task(timeNow, timeNow.plusMinutes(T2), "not ready", 1);
                }

                tasks.add(task);
                LOGGER.info("Task #" + task.getNum() + " create");
                time = time.now();
            }

            for (Task thisTask : tasks) {
                if ((thisTask.getStartTime().plusMinutes(T2 - T3).getMinute() <= timeNow.getMinute())
                        & (thisTask.getStatus().equals("not ready"))) {
                    thisTask.setStatus("FIRE");
                    LOGGER.info("Task #" + thisTask.getNum() + " FIRE!");
                }
                if (thisTask.getStartTime().plusMinutes(T2).getMinute() <= timeNow.getMinute()) {
                    thisTask.setStatus("end");
                    LOGGER.info("Task #" + thisTask.getNum() + " not completed!");
                }
            }

            for (int i = 0; i < tasks.size(); i++) {
                if ((tasks.get(i).getStatus().equals("end")) || (tasks.get(i).getStatus().equals("ready"))) {
                    tasks.remove(i);
                }
            }
        }
    }

    public void getTaskInformation() {
        int i = 1;
        if (tasks.size() != 0) {
            for (Task thisTask : tasks) {
                LocalTime time = null;
                System.out.println("Task � " + thisTask.getNum());
                System.out.println("(task number in task's list " + i + ")");

                if ((thisTask.getStatus().equals("not ready")) || (thisTask.getStatus().equals("FIRE"))) {
                    System.out.println("Status: " + thisTask.getStatus());
                    int leftTime = thisTask.getEndTime().getMinute() - time.now().getMinute();
                    System.out.println("Time left: " + leftTime + " minutes");
                }

                i++;
                System.out.println("----------------------------------");
            }
        } else {
            System.out.println("no task now");
        }
    }

    @Override
    public void completeTask() {
        Scanner in = new Scanner(System.in);
        System.out.println("What is the task?");
        int num = 0;
        try {
            num = in.nextInt();
        } catch (InputMismatchException e) {
            LOGGER.error("InputMismatchException");
        }

        if (((num - 1) >= 0) && ((num - 1) < tasks.size())) {
            tasks.get(num - 1).setStatus("ready");
            LOGGER.info("Task #" + tasks.get(num - 1).getNum() + " completed!");
            System.out.println("task completed!");
        } else {
            System.out.println("wrong number task");
        }
    }


}
