package ru.omsu;

import java.time.LocalTime;

public abstract class Work {
    protected Task task;
    protected int T1; // ��������
    protected int T2; //�������
    protected int T3; //�����
    protected LocalTime time;

    abstract void getTaskInformation();

    void completeTask() {
        task.setStatus("ready");
    }

    abstract void stopWork();


    public Work(Task task, LocalTime time, int t1, int t2, int t3) {
        T1 = t1;
        T2 = t2;
        T3 = t3;
        this.time = time;
        this.task = task;
    }


}
