package ru.omsu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import static javafx.application.Platform.exit;

public class Main {

    private static Work work;
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkMultiTask.class);

    public static List<Integer> readFromFile(String fileName) {
        List<Integer> result = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            result.add(Integer.valueOf(br.readLine()));
            result.add(Integer.valueOf(br.readLine()));
            result.add(Integer.valueOf(br.readLine()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LOGGER.error("FileNotFoundException");
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("IOException");
        }
        return result;
    }

    public static void main(String[] arr) {
        Scanner in = new Scanner(System.in);
        int choice = 0;
        LocalTime time = null;
        System.out.println("enter the path to file with parameters");
        String path = in.nextLine();
        List<Integer> param = readFromFile(path);


        work = new WorkMultiTask(new Task(time.now(), time.now().plusMinutes(param.get(1)), "not ready", 1),
                                time.now(), param.get(0), param.get(1), param.get(2));


        while (choice != 3) {

            System.out.println(" ");
            System.out.println("1.information about task");
            System.out.println("2. make task");
            System.out.println("3. Exit");
            System.out.println(" ");
            try {
                choice = in.nextInt();
            } catch (InputMismatchException e) {
                e.printStackTrace();
                LOGGER.error("InputMismatchException");
            }
            if (choice == 1) {
                work.getTaskInformation();
            } else if (choice == 2) {
                work.completeTask();
            } else if (choice == 3) {
                work.stopWork();
                exit();
            } else {
                System.out.println("wrong number");
            }
        }
    }


}
